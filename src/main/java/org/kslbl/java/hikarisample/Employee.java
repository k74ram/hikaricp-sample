package org.kslbl.java.hikarisample;

import java.util.Date;

import lombok.Builder;
import lombok.Data;

/**
 * @author kholid
 */
@Data
@Builder
public class Employee {

        private int empNo;
        private String ename;
        private String job;
        private int mgr;
        private Date hiredate;
        private int sal;
        private int comm;
        private int deptno;
    
}