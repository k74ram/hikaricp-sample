package org.kslbl.java.hikarisample;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App {
    public static List<Employee> fetchData() {
        final String SQL_QUERY = "select * from emp";
        List<Employee> employees = null;
        try (Connection con = DataSource.getConnection();
                PreparedStatement pst = con.prepareStatement(SQL_QUERY);
                ResultSet rs = pst.executeQuery();) {
            employees = new ArrayList<Employee>();
            Employee employee;
            while (rs.next()) {
                employee = Employee.builder()
                   .empNo(rs.getInt("empno"))
                   .ename(rs.getString("ename"))
                   .job(rs.getString("job"))
                   .mgr(rs.getInt("mgr"))
                   .hiredate(rs.getDate("hiredate"))
                   .sal(rs.getInt("sal"))
                   .comm(rs.getInt("comm"))
                   .deptno(rs.getInt("deptno"))
                   .build();
                employees.add(employee);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } 
        return employees;
    }
    
    public static void main( String[] args ) throws InterruptedException
    {
        fetchData().stream().forEach(System.out::println);
    }
}
